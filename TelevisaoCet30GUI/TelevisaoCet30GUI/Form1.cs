﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTV;

        public Form1()
        {
            InitializeComponent();
            minhaTV = new Tv();

            LabelStatus.Text = minhaTV.Mensagem;
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (!minhaTV.GetEstado())
            {
                minhaTV.LigaTv();
                LabelStatus.Text = minhaTV.Mensagem;
               
                ButtonOnOff.Text = "OFF";
                ButtonAumentaCanal.Enabled = true;
                ButtonDiminuiCanal.Enabled = true;
                LabelCanal.Text = minhaTV.Canal.ToString();

                TrackBarVolume.Enabled = true;
                LabelVolume.Text = minhaTV.Volume.ToString();
                TrackBarVolume.Value = minhaTV.Volume;

            }
            else
            {
                minhaTV.DesligaTv();
                LabelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "ON";
                ButtonAumentaCanal.Enabled = false;
                ButtonDiminuiCanal.Enabled = false;
                LabelCanal.Text = "- -";

                TrackBarVolume.Enabled = false;
                LabelVolume.Text = "- -";
            }

           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            minhaTV.Canal --;
            LabelCanal.Text = minhaTV.Canal.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            minhaTV.Canal++;
            LabelCanal.Text = minhaTV.Canal.ToString();
        }

        private void LabelCanal_Click(object sender, EventArgs e)
        {

        }

        private void TrackBarVolume_Scroll(object sender, EventArgs e)
        {

            minhaTV.Volume =  TrackBarVolume.Value;

            LabelVolume.Text = minhaTV.Volume.ToString();

        }
    }
}
